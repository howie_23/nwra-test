Northwest Registered Agent Test by Kelly Hauenstein
E-mail: howie@howie23.org
-------------------------------
All commands must take place in the root directory

Requirements: Node.js, npm, grunt.js, bower

To install grunt plugins:
'npm install'

To install all dependencies:
'bower install'

To compile final code:
'grunt'