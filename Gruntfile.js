module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        //Compile Sass
        sass: {
            dev: {
                options: {
                    style: 'expanded',
                    loadPath: 'vendor/foundation/scss',
                },
                files: {
                    'dist/styles/style.css': 'src/styles/style.scss'
                }
            },
            dist: {
                options: {
                    style: 'compressed',
                    loadPath: 'vendor/foundation/scss',
                    sourcemap: 'none',
                },
                files: {
                    'dist/styles/style.css': 'src/styles/style.scss'
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie 9'],
            },
            dist: {
                expand: true,
                src: 'dist/styles/style.css'
            }
        },
        cssmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'dist/styles',
                    src: '*.css',
                    dest: 'dist/styles',
                    ext: '.css'
                }]
            }
        },
        concat: {
            files: {
                src: ['vendor/fastclick/lib/fastclick.js','vendor/foundation/js/foundation/foundation.js','vendor/foundation/js/foundation/foundation.topbar.js'],
                dest: 'dist/scripts/global.js'
            }
        },
        uglify: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'dist/scripts',
                    src: '**/*.js',
                    dest: 'dist/scripts'
                }]
            }
        },
        copy: {
            main: {
                files: [
                    {expand: true, flatten: true, src: 'vendor/modernizr/modernizr.js', dest: 'dist/scripts/'},
                ],
            },
        },
        watch: {
            css: {
                files: ['src/styles/*.scss'],
                tasks: ['sass:dev'],
                options: {
                    spawn: false,
                }
            },
        }
    });
    //Load Grunt Tasks
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    //Tasks
    grunt.registerTask('default', ['sass:dist','autoprefixer','cssmin','concat','uglify','copy']);
}